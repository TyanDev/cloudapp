package net.krudde.cloudapp.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import net.krudde.cloudapp.R;
import net.tyan.cloudapi.CloudAPI;
import net.tyan.cloudapi.reference.Operations;


/**
 * Created by Kevin
 */
public class TeamSpeak3Fragment extends Fragment {

    View contentView;
    TextView status;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        contentView = inflater.inflate(R.layout.teamspeak3fragment_layout, null);

        return contentView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        status = (TextView) contentView.findViewById(R.id.status);
        CloudAPI.send(Operations.TEAMSPEAK_STATUS);
        refresh(500);

        Button restartTs3 = (Button) contentView.findViewById(R.id.restart_ts3);
        restartTs3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "TeamSpeak 3 Server wird neugestartet!", Toast.LENGTH_SHORT).show();
                CloudAPI.send(Operations.RESTART_TEAMSPEAK);
            }
        });
        Button startTs3 = (Button) contentView.findViewById(R.id.start_ts3);
        startTs3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "TeamSpeak 3 Server wird gestartet!", Toast.LENGTH_SHORT).show();
                CloudAPI.send(Operations.START_TEASPEAK);
                refresh(11000);
            }
        });
        Button stopTs3 = (Button) contentView.findViewById(R.id.stop_ts3);
        stopTs3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "TeamSpeak 3 Server wird gestoppt!", Toast.LENGTH_SHORT).show();
                CloudAPI.send(Operations.STOP_TEAMSPEAK);
                refresh(1000);
            }
        });
        final Button refresh = (Button) contentView.findViewById(R.id.refresh);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "Refreshing...", Toast.LENGTH_SHORT).show();
                refresh(1000);
            }
        });

        super.onViewCreated(view, savedInstanceState);
    }

    private void refresh(int delay) {
        CloudAPI.send(Operations.TEAMSPEAK_STATUS);

        Handler handler = new Handler();
        final Runnable r = new Runnable() {
            @Override
            public void run() {
                if (CloudAPI.getTeamSpeakStatus() == 0) {
                    status.setText("Online");
                    status.setTextColor(Color.GREEN);
                } else {
                    status.setText("Offline");
                    status.setTextColor(Color.RED);
                }
            }
        };
        handler.postDelayed(r, delay);

    }
}
