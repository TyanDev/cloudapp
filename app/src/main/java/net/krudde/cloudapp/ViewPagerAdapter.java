package net.krudde.cloudapp;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import net.krudde.cloudapp.fragments.AboutFragment;
import net.krudde.cloudapp.fragments.TeamSpeak3Fragment;
import net.krudde.cloudapp.fragments.VPSFragment;

/**
 * Created by Kevin
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {

    String[] tabTitles = {"TeamSpeak 3", "VPS", "Über"};

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {

            case 0:
                return new TeamSpeak3Fragment();
            case 1:
                return new VPSFragment();
            case 2:
                return new AboutFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return tabTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
